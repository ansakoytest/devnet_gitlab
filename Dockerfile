FROM centos:7

COPY api.py /opt/python_api/api.py
COPY requirements.txt /opt/python_api/requirements.txt
COPY test.py /opt/python_api/test.py

RUN yum update -y \
    && yum -y install gcc openssl-devel bzip2-devel libffi-devel zlib-devel xz-devel \
    && yum -y install wget make \
    && wget https://www.python.org/ftp/python/3.9.6/Python-3.9.6.tgz \
    && tar xzf Python-3.9.6.tgz \
    && cd Python-3.9.6 \
    && ./configure --enable-optimizations \
    && make altinstall \
    && cd .. && rm Python-3.9.6.tgz \
    && yum install python3-pip -y \
    && yum clean all \
    && python3.9 -m pip install --upgrade pip \
    && python3.9 -m pip install -r /opt/python_api/requirements.txt

ENTRYPOINT  ["python3.9", "/opt/python_api/api.py"]
