import json

from api import app


def get_data(response):
    return json.loads(response.get_data)


def test_response_get_info_ok():
    response = app.test_client().get("/get_info")
    assert response.status_code == 200


def test_response_get_info_message():
    response = app.test_client().get("/get_info")
    data = json.loads(response.get_data(as_text=True))
    assert data["message"] == "Running"


if __name__ == '__main__':
    test_response_get_info_ok()
    test_response_get_info_message()
